﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSiteCall
{
    public class HttpCall
    {
        private string siteUrl;
        private int threadCnt;
        private int callCnt;

        private static int totalCnt = 0;

        public HttpCall(string siteUrl ,int threadCnt, int callCnt)
        {
            this.siteUrl = siteUrl;
            this.threadCnt = threadCnt;
            this.callCnt = callCnt;
        }

        public void executeThread()
        {
            Thread[] ts = new Thread[threadCnt];

            for (int i=0;i< threadCnt;i++)
            {
                ts[i] = new Thread(() =>
                {
                    call();
                });

                ts[i].Start();
            }
        }

        private void call()
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;

            for (int i=0;i<callCnt;i++)
            {
                try
                {
                    totalCnt++;

                    request = (HttpWebRequest)WebRequest.Create(siteUrl);
                    request.Method = "GET";
                    response = (HttpWebResponse)request.GetResponse();
                    response.Close();

                    //Console.WriteLine(totalCnt);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }
    }
}
