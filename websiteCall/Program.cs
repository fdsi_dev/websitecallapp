﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSiteCall
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HttpCall httpCall = new HttpCall("https://www.vip-7979.com", 20, 5);
                int startMin = DateTime.Now.Minute;
                int result=0;
            
             
                while(true)
                {
                    httpCall.executeThread();
                    result = DateTime.Now.Minute - startMin;

                    Console.WriteLine(result);

                    if(result == 1)
                        break;                    
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
